extern crate petgraph;
extern crate slotmap;
extern crate slab;

use petgraph::Graph;
use std::default::Default;
use slab::Slab;

#[allow(dead_code)]

#[derive(Default,Copy,Clone)]
struct Tag {
    name: &'static str,
    value: i64,
}

#[derive(Clone)]
struct Instruction {
    name: String,
    data_type: InstructionType,
    tag: Option<usize>,
}

struct Rung {
    ins_id_paths: Vec<Vec<usize>>,
}

#[derive(PartialEq,Copy,Clone)]
enum InstructionType {
    XIC,
    XIO,
    OTE,
    OTL,
    OTU,
}


// find_unqiue_path recsursively run the graph
fn find_unqiue_paths(rung: &mut Rung, graph: &Graph<usize, usize>, node: petgraph::graph::NodeIndex, path: &mut Vec<usize>) {

    let children = graph.neighbors(node).peekable();
    match graph.neighbors(node).peekable().peek() {

        Some(_) =>
            for n in children {
                let mut new_path = path.clone();
                new_path.push(graph[n].clone());
                find_unqiue_paths(rung, &graph, n, &mut new_path);
            },
    
        None => {
            rung.ins_id_paths.push(path.to_vec());
            println!("{:?}", rung.ins_id_paths);
        },
            
    }   
    
}

fn print_rung(prog: &mut ProgramState, rung: &Rung) {
    for path in rung.ins_id_paths.iter().rev() {
        for ins_id in path.iter() {
            println!("{:?}",prog.get_instruction_tag(*ins_id).name);
        }
    }
}

fn evaluate_rung(prog: &mut ProgramState, rung: &Rung) {
    for path in rung.ins_id_paths.iter().rev() {
        for ins_id in path.iter() {

            let mut tag = prog.get_instruction_tag(*ins_id);
            let tag_val = prog.get_instruction_tag(*ins_id).value;

            let ins_evaluation = match prog.instruction[*ins_id].data_type {

                InstructionType::XIC => {
                    if tag_val == 1 {
                        true
                    } else {
                        false
                    }
                },

                InstructionType::XIO => {
                    if tag_val == 1 {
                        false
                    } else {
                        true
                    }
                },

                InstructionType::OTE => {
                    if tag_val == 0 {
                        tag.value = 1;
                        true
                    } else {
                        true
                    }
                },

                InstructionType::OTL => {
                    if tag_val == 0 {
                        tag.value = 1;
                        true
                    } else {
                        true
                    }
                },

                InstructionType::OTU => {
                    if tag_val == 1 {
                        tag.value = 0;
                        true
                    } else {
                        true
                    }
                },
            };

            println!("{:?}",prog.instruction[*ins_id].name);
            println!("{:?}",ins_evaluation);
            println!("{:?}",prog.get_instruction_tag(*ins_id).name);
            if !ins_evaluation {
                break
            }
        }
    }    
}


pub struct ProgramState {
    tag: Slab<Tag>,
    instruction: Slab<Instruction>,
    rung: Slab<Rung>,
}

impl ProgramState {
    fn new() -> ProgramState {
        ProgramState {
            tag: Slab::new(),
            instruction: Slab::new(),
            rung: Slab::new(),
        }
    }

    fn new_tag(&mut self, name: &'static str) -> usize {
        let t = Tag { name: name, value: 0 };
        self.tag.insert(t)
    }

    fn new_instruction(&mut self, name: &'static str, data_type: InstructionType, tag_ref: Option<usize>) -> usize {
        let i = Instruction { name: name.to_string(), data_type: InstructionType::XIC, tag: None };
        self.instruction.insert(i)
    }

    fn get_instruction_tag(&self, instruction_ref: usize) -> Tag {
        self.tag[self.instruction[instruction_ref].tag.unwrap()]
    }
}


fn main() {

    let mut prog = ProgramState::new();

    let mut t = prog.new_tag("A");
    let mut t2 = prog.new_tag("B");

    prog.tag[t].value = 1;
    prog.tag[t2].value = 0;

    let mut i = prog.new_instruction("a", InstructionType::XIC, Some(t));
    let mut i2 = prog.new_instruction("b", InstructionType::XIC, Some(t2));
    let mut i3 = prog.new_instruction("c", InstructionType::XIC, Some(t));
    let mut i4 = prog.new_instruction("d", InstructionType::OTE, Some(t2));

    prog.instruction[i].tag = Some(t);
    prog.instruction[i2].tag = Some(t2);
    prog.instruction[i3].tag = Some(t);
    
    let mut graph = Graph::<usize, usize>::new();
    let _a = graph.add_node(i);
    let _b = graph.add_node(i2);
    let _c = graph.add_node(i3);
    let _d = graph.add_node(i);
    let _e = graph.add_node(i2);
    let _f = graph.add_node(i3);
    let _g = graph.add_node(i4);
    let _h = graph.add_node(i);
    let _i = graph.add_node(i);
    
    graph.extend_with_edges(&[
        (_a, _b),
        (_a, _f),
        (_b, _c),
        (_f, _c),
        (_c, _d),
        (_d, _e),
        (_c, _g),
        (_c, _h),
        (_h, _i),
        (_g, _e),
    ]);

    let mut first_rung = Rung { ins_id_paths: Vec::new() };
    find_unqiue_paths(&mut first_rung, &graph, _a, &mut vec![i]);
    evaluate_rung(&mut prog, &first_rung);
    prog.tag[t].value = 0;
    evaluate_rung(&mut prog, &first_rung);

}
